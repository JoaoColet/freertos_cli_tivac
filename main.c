#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"

#include "FreeRTOS.h"
#include "task.h"

#include "queue.h"
#include "semphr.h"

#include "inc/hw_ints.h"
#include "inc/hw_uart.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"

#include "FreeRTOS_CLI.h"
#include "FreeRTOS_IO.h"
#include "FreeRTOS_uart.h"

#include "lwiplib.h"
#include "pinout.h"
#include "telnet_server.h"
#include "httpd.h"
#include "sntp.h"

extern void SocketTCPClient( void *pvParameters);

#define cmdPARAMETER_NOT_USED  ((void * ) 0 )
#define MAX_INPUT_LENGHT    50
#define MAX_OUTPUT_LENGHT   512

portBASE_TYPE UARTGetChar(char *data, TickType_t timeout);
void UARTPutChar(uint32_t ui32Base, char ucData);
void UARTPutString(uint32_t ui32Base, char *string);
void task1(void *param);
void task2(void *param);

TaskHandle_t taskTerminal_handle;
TaskHandle_t task1_handle;
TaskHandle_t task2_handle;

uint32_t g_ui32SysClock;

static int LED2_Blink_Time = 1000;

//comando para alterar a frequencia do led da task2
static BaseType_t prvLedBlinkTime(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    char *stringoutput;
    (void) xWriteBufferLen;

    char *pcParameter;
    BaseType_t xParameterStringLength;
    pcParameter = FreeRTOS_CLIGetParameter(pcCommandString,1,&xParameterStringLength);
    int blinktime = atoi(pcParameter);
    if(blinktime >=500 && blinktime <=10000){
        LED2_Blink_Time = blinktime;
        stringoutput = strcat(pcParameter, " ms is new time of LED 2\n\r\n\r");
    }
    else{
        stringoutput = "\r\nValue out of range, input a value between 500 and 10000\n\r\n\r";
    }
    strcpy(pcWriteBuffer, stringoutput);
    return pdFALSE;
}

static const CLI_Command_Definition_t xLedBlinkTime = {
     "blinktime",
     "blinktime VALUE -- VALUE is time in ms of blink LED, the range is 500 to 10000 \r\n\r\n",
     prvLedBlinkTime,
     1
};

//comando para desligar o led da task1 e suspender a tarefa
static BaseType_t prvTaskTurnLedOff(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    char *stringoutput;
    (void) xWriteBufferLen;

    TaskStatus_t xTaskDetails;
        vTaskGetInfo( /* The handle of the task being queried. */
                          task1_handle,
                          /* The TaskStatus_t structure to complete with information
                          on xTask. */
                          &xTaskDetails,
                          /* Include the stack high water mark value in the
                          TaskStatus_t structure. */
                          pdTRUE,
                          /* Include the task state in the TaskStatus_t structure. */
                          eInvalid );

    if(xTaskDetails.eCurrentState == eBlocked || xTaskDetails.eCurrentState == eReady){
        stringoutput = "\n\rLED 1 turn off, task1 suspended\n\r\n\r";
        vTaskSuspend(task1_handle);
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0);
    }
    else{
        stringoutput = "\n\rLED 1 is already off\n\r\n\r";
    }
    strcpy(pcWriteBuffer, stringoutput);

    return pdFALSE;
}

static const CLI_Command_Definition_t xTaskTurnLedOff = {
     "ledoff",
     "ledoff  -- turn off LED1\r\n\r\n",
     prvTaskTurnLedOff,
     0
};

//comando para ligar o led da task 1
static BaseType_t prvTaskTurnLedOn(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    char *stringoutput;
    (void) xWriteBufferLen;

    TaskStatus_t xTaskDetails;
    vTaskGetInfo( /* The handle of the task being queried. */
                      task1_handle,
                      /* The TaskStatus_t structure to complete with information
                      on xTask. */
                      &xTaskDetails,
                      /* Include the stack high water mark value in the
                      TaskStatus_t structure. */
                      pdTRUE,
                      /* Include the task state in the TaskStatus_t structure. */
                      eInvalid );

    if(xTaskDetails.eCurrentState == eSuspended){
        stringoutput = "\r\nLED 1 turn on, task1 resumed\n\r\n\r";
        vTaskResume(task1_handle);
    }
    else{
        stringoutput = "\r\nLED 1 is already on\n\r\n\r";
    }
    strcpy(pcWriteBuffer, stringoutput);


    return pdFALSE;
}

static const CLI_Command_Definition_t xTaskTurnLedOn = {
     "ledon",
     "ledon  -- turn on LED1\r\n\r\n",
     prvTaskTurnLedOn,
     0
};

//comando para listar todas as tarefas do sistema
static BaseType_t prvTaskStatsCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    static BaseType_t state = 0;

    if(!state){
        char *head = "\n\rName                          State  Priority   Stack   Number\n\r";
        (void) xWriteBufferLen;

        strcpy(pcWriteBuffer, head);
        vTaskList(&pcWriteBuffer[strlen(head)]);

        state = 1;
        return pdTRUE;
    }
    else{
        state = 0;
        strcpy(pcWriteBuffer, "\n\r");
        return pdFALSE;
    }
}

static const CLI_Command_Definition_t xTasksCommand = {
 "tasks",
 "tasks -- Lists all the installed tasks\r\n\r\n",
 prvTaskStatsCommand,
 0
};

void task1(void *param){
    // Enable the GPIO port that is used for the on-board LED.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);

    // Check if the peripheral access is enabled.
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPION)){
        vTaskDelay(100);
    }

    // Enable the GPIO pin for the LED (PN0).  Set the direction as output, and
    // enable the GPIO pin for digital function.
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0);

    while(1){
        // Turn on the LED.
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, GPIO_PIN_0);
        vTaskDelay(1000);

        // Turn off the LED.
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0, 0);
        vTaskDelay(1000);
    }
}

void task2(void *param){
    // Enable the GPIO port that is used for the on-board LED.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);

    // Check if the peripheral access is enabled.
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPION)){
        vTaskDelay(100);
    }

    // Enable the GPIO pin for the LED (PN1).  Set the direction as output, and
    // enable the GPIO pin for digital function.
    GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_1);

    while(1){
        // Turn on the LED.
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, GPIO_PIN_1);
        vTaskDelay(LED2_Blink_Time);

        // Turn off the LED.
        GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, 0);
        vTaskDelay(LED2_Blink_Time);
    }
}

void Terminal(void *param);

uint32_t g_ui32SysClock;
struct netif sNetIF;
volatile BaseType_t lwip_link_up = pdFALSE;
uint32_t g_ui32IPAddress;
void lwIPHostTimerHandler(void);
void DisplayIPAddress(uint32_t ui32Addr);
void UpLwIP(void *param);

QueueHandle_t qUART0;
QueueHandle_t qUART0Tx;

SemaphoreHandle_t sUART0;
SemaphoreHandle_t mutexTx0;

portBASE_TYPE UARTGetChar(char *data, TickType_t timeout);
void UARTPutChar(uint32_t ui32Base, char ucData);
void UARTPutString(uint32_t ui32Base, char *string);

int main(void)
{

    MAP_SysCtlMOSCConfigSet(SYSCTL_MOSC_HIGHFREQ);

    g_ui32SysClock = MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_240), 120000000);

    MAP_FPUEnable();
    MAP_FPULazyStackingEnable();

    //comandos registrados no CLI
    FreeRTOS_CLIRegisterCommand(&xTaskTurnLedOff);
    FreeRTOS_CLIRegisterCommand(&xTaskTurnLedOn);
    FreeRTOS_CLIRegisterCommand(&xLedBlinkTime);
    FreeRTOS_CLIRegisterCommand(&xTasksCommand);

    //tarefas do sistema
    xTaskCreate(task1, "LED 1", 256, NULL, 10, &task1_handle);
    xTaskCreate(task2, "LED 2", 256, NULL, 10, &task2_handle);

    xTaskCreate(Terminal, "Terminal Serial", 256, NULL, 6, &taskTerminal_handle);
    xTaskCreate(UpLwIP, "LwIP Task", 1024, NULL, 4, NULL);

    vTaskStartScheduler();

    return 0;
}

void Terminal(void *param){
    char data;
    (void)param;
    //inicizliza��o da comunica��o UART
    sUART0 = xSemaphoreCreateBinary();

    if(sUART0 == NULL){
        vTaskSuspend(NULL);
    }
    else{
        mutexTx0 = xSemaphoreCreateMutex();
        if(mutexTx0 == NULL){
            vSemaphoreDelete(sUART0);
            vTaskSuspend(NULL);
        }
        else{
            qUART0 = xQueueCreate(128, sizeof(char));
            if(qUART0 == NULL){
                vSemaphoreDelete(sUART0);
                vSemaphoreDelete(mutexTx0);
                vTaskSuspend(NULL);
            }
            else{
                qUART0Tx = xQueueCreate(128, sizeof(char));

                MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
                MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

                MAP_GPIOPinConfigure(GPIO_PA0_U0RX);
                MAP_GPIOPinConfigure(GPIO_PA1_U0TX);
                MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

                MAP_UARTConfigSetExpClk(UART0_BASE, configCPU_CLOCK_HZ, 115200, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

                MAP_UARTFIFODisable(UART0_BASE);

                MAP_IntPrioritySet(INT_UART0, 0xC0);
                MAP_IntEnable(INT_UART0);
                MAP_UARTIntEnable(UART0_BASE, UART_INT_RX);
            }
        }
    }

    UARTPutString(UART0_BASE, "\033[2J\033[H");

    UARTPutString(UART0_BASE, "FreeRTOS started\n\r");

    //la�o infinito fazendo echo da UART
    while(1){
        (void)UARTGetChar(&data, portMAX_DELAY);
        if(data!=13){
            UARTPutChar(UART0_BASE, data);
        }
        else{
            UARTPutChar(UART0_BASE, '\n');
            UARTPutChar(UART0_BASE, '\r');
        }
    }
}
/*
*   Fun��es UART usadas no terminal serial
*   o terminal UART imprimira o IP do tiva para acessar o telnet na porta 23
*   no exemplo do FreeRTOS+CLI na UART usei as fun��es do FreeRTOS+IO (FreeRTOSread e FreeRTOSwrite)
*   porem ao juntar o exemplo do telnet as fun��es do lwip n�o funcionavam mais ent�o usei
*   as fun��es UART criadas nas aulas.
*/
volatile uint32_t isstring = 0;

void UARTIntHandler(void){
    uint32_t ui32status;
    signed portBASE_TYPE pxHigherPriorityTaskWokenRX = pdFALSE;
    signed portBASE_TYPE pxHigherPriorityTaskWokenTX = pdFALSE;
    char data;


    BaseType_t ret;


    ui32status = MAP_UARTIntStatus(UART0_BASE, true);

    UARTIntClear(UART0_BASE, ui32status);

    if((ui32status&UART_INT_RX) == UART_INT_RX){
        while(MAP_UARTCharsAvail(UART0_BASE)){
            data = (char)MAP_UARTCharGetNonBlocking(UART0_BASE);
            xQueueSendToBackFromISR(qUART0, &data, &pxHigherPriorityTaskWokenRX);
        }
    }

    if((ui32status&UART_INT_TX) == UART_INT_TX){

        if(isstring){
            ret = xQueueReceiveFromISR(qUART0Tx, &data, NULL);

            if(ret){
                HWREG(UART0_BASE + UART_O_DR) = data;
            }
            else{
                isstring = 0;
                MAP_UARTIntDisable(UART0_BASE, UART_INT_TX);

                xSemaphoreGiveFromISR(sUART0, &pxHigherPriorityTaskWokenTX);
            }
        }
        else{
            MAP_UARTIntDisable(UART0_BASE, UART_INT_TX);
            xSemaphoreGiveFromISR(sUART0, &pxHigherPriorityTaskWokenTX);
        }
    }

    if((pxHigherPriorityTaskWokenRX == pdTRUE) || (pxHigherPriorityTaskWokenTX == pdTRUE)){
        portYIELD();
    }
}

portBASE_TYPE UARTGetChar(char *data, TickType_t timeout){
    return xQueueReceive(qUART0, data, timeout);
}


void UARTPutChar(uint32_t ui32Base, char ucData){
    if(mutexTx0 != NULL){
        if(xSemaphoreTake(mutexTx0, portMAX_DELAY) == pdTRUE){
            HWREG(ui32Base + UART_O_DR) = ucData;

            MAP_UARTIntEnable(UART0_BASE, UART_INT_TX);

            xSemaphoreTake(sUART0, portMAX_DELAY);
            xSemaphoreGive(mutexTx0);
        }
    }
}


void UARTPutString(uint32_t ui32Base, char *string){
    char data;
    if(mutexTx0 != NULL){
        if(xSemaphoreTake(mutexTx0, portMAX_DELAY) == pdTRUE){
            isstring=1;
            while(*string){
                xQueueSendToBack(qUART0Tx, string, portMAX_DELAY);

                string++;
            }

            xQueueReceive(qUART0Tx, &data, portMAX_DELAY);
            HWREG(ui32Base + UART_O_DR) = data;

            MAP_UARTIntEnable(UART0_BASE, UART_INT_TX);

            xSemaphoreTake(sUART0, portMAX_DELAY);

            xSemaphoreGive(mutexTx0);
        }
    }
}

//*****************************************************************************
//
// Display an lwIP type IP Address.
//
//*****************************************************************************
void
DisplayIPAddress(uint32_t ui32Addr)
{
    char pcBuf[16];

    //
    // Convert the IP Address into a string.
    //
    sprintf(pcBuf, "%d.%d.%d.%d", (int)(ui32Addr & 0xff), (int)((ui32Addr >> 8) & 0xff),
            (int)((ui32Addr >> 16) & 0xff), (int)((ui32Addr >> 24) & 0xff));

    //
    // Display the string.
    //
    UARTPutString(UART0_BASE, pcBuf);
}


//*****************************************************************************
//
// Required by lwIP library to support any host-related timer functions.
//
//*****************************************************************************
void
lwIPHostTimerHandler(void)
{
    uint32_t ui32Idx, ui32NewIPAddress;

    //
    // Get the current IP address.
    //
    ui32NewIPAddress = lwIPLocalIPAddrGet();

    //
    // See if the IP address has changed.
    //
    if(ui32NewIPAddress != g_ui32IPAddress)
    {
        //
        // See if there is an IP address assigned.
        //
        if(ui32NewIPAddress == 0xffffffff)
        {
            //
            // Indicate that there is no link.
            //
            UARTPutString(UART0_BASE, "Waiting for link.\n\r");
        }
        else if(ui32NewIPAddress == 0)
        {
            //
            // There is no IP address, so indicate that the DHCP process is
            // running.
            //
            UARTPutString(UART0_BASE, "Waiting for IP address.\n\r");
        }
        else
        {
            //
            // Display the new IP address.
            //
            lwip_link_up = pdTRUE;
            UARTPutString(UART0_BASE, "IP Address: ");
            DisplayIPAddress(ui32NewIPAddress);
            UARTPutString(UART0_BASE, "\n\rOpen a browser and enter the IP address.\n\r");
            UARTPutString(UART0_BASE, "Open a telnet and enter the IP address in port 23.\n\r");
        }

        //
        // Save the new IP address.
        //
        g_ui32IPAddress = ui32NewIPAddress;

        //
        // Turn GPIO off.
        //
       // MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1, ~GPIO_PIN_1);
    }

    //
    // If there is not an IP address.
    //
    if((ui32NewIPAddress == 0) || (ui32NewIPAddress == 0xffffffff))
    {
        //
        // Loop through the LED animation.
        //

        for(ui32Idx = 1; ui32Idx < 17; ui32Idx++)
        {

            //
            // Toggle the GPIO
            //
#if 0
            MAP_GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_1,
                    (MAP_GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_1) ^
                     GPIO_PIN_1));

           // DelayTask(1000/ui32Idx);
#endif
        }
    }
}

void UpLwIP(void *param)
{
    uint32_t ui32User0, ui32User1;
    //uint32_t ui32Loop;
    uint8_t pui8MACArray[6];
    (void)param;
    //
    // Configure the device pins.
    //
    PinoutSet(true, false);

    vTaskDelay(1500);
    UARTPutString(UART0_BASE, "Ethernet lwIP example\n\r");

    // Configure the hardware MAC address for Ethernet Controller filtering of
    // incoming packets.  The MAC address will be stored in the non-volatile
    // USER0 and USER1 registers.
    ui32User0 = 0x001AB6;
    ui32User1 = 0x0318CC;

    MAP_FlashUserSet(ui32User0, ui32User1);
    MAP_FlashUserGet(&ui32User0, &ui32User1);
    if((ui32User0 == 0xffffffff) || (ui32User1 == 0xffffffff)){
        // We should never get here.  This is an error if the MAC address has
        // not been programmed into the device.  Exit the program.
        // Let the user know there is no MAC address
        UARTPutString(UART0_BASE, "No MAC programmed!\n\r");
        while(1)
        {
        }
    }

    // Tell the user what we are doing just now.
    UARTPutString(UART0_BASE, "Waiting for IP.\n\r");

    // Convert the 24/24 split MAC address from NV ram into a 32/16 split MAC
    // address needed to program the hardware registers, then program the MAC
    // address into the Ethernet Controller registers.
    pui8MACArray[0] = ((uint8_t)(ui32User0 >>  0) & 0xff);
    pui8MACArray[1] = ((uint8_t)(ui32User0 >>  8) & 0xff);
    pui8MACArray[2] = ((uint8_t)(ui32User0 >> 16) & 0xff);
    pui8MACArray[3] = ((uint8_t)(ui32User1 >>  0) & 0xff);
    pui8MACArray[4] = ((uint8_t)(ui32User1 >>  8) & 0xff);
    pui8MACArray[5] = ((uint8_t)(ui32User1 >> 16) & 0xff);

    // Initialize the lwIP library, using DHCP.
    lwIPInit(configCPU_CLOCK_HZ, pui8MACArray, 0, 0, 0, IPADDR_USE_DHCP, &sNetIF);

    // Set the interrupt priorities.  We set the SysTick interrupt to a higher
    // priority than the Ethernet interrupt to ensure that the file system
    // tick is processed if SysTick occurs while the Ethernet handler is being
    // processed.  This is very likely since all the TCP/IP and HTTP work is
    // done in the context of the Ethernet interrupt.
    while(lwip_link_up != pdTRUE){
        vTaskDelay(1000);
    }

    // Initialize a sample httpd server.
    httpd_init();

    // Inicia cliente SNTP
    sntp_init();

    sys_thread_new("LwIP Telnet Server", SocketTelnetServer, NULL, 2048, 5);


    // Loop forever.  All the work is done in the created tasks
    while(1)
    {
        // Delay ou pode inclusive apagar a tarefa
        vTaskDelay(10000);
    }

}
